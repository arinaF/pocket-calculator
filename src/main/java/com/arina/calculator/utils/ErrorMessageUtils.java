package com.arina.calculator.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ErrorMessageUtils {

    public static void createErrorMessage(String summary, String details) {
        FacesContext.getCurrentInstance()
                .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, details));
    }
}
