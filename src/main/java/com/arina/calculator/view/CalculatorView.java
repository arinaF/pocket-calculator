package com.arina.calculator.view;

import com.arina.calculator.bean.CalculatorManagedBean;
import com.arina.calculator.bean.LogManagedBean;
import com.arina.calculator.bean.LoginManagedBean;
import com.arina.calculator.model.CalcUser;
import com.arina.calculator.utils.ErrorMessageUtils;
import org.apache.commons.lang3.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;

@ManagedBean
@ViewScoped
public class CalculatorView {

    private CalcUser user;
    private boolean editMode;
    private StringBuilder logs;
    private String logName;

    private String expression = "";
    private String operand = "";
    private String operator = null;
    private BigDecimal result = BigDecimal.valueOf(0);


    @ManagedProperty(value = "#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;

    @ManagedProperty(value = "#{calculatorManagedBean}")
    private CalculatorManagedBean calculatorManagedBean;

    @ManagedProperty(value = "#{logManagedBean}")
    private LogManagedBean logManagedBean;

    public String login() {
        return "/public/login.xhtml?faces-redirect=true";
    }

    public String signup() {
        return "/public/signup.xhtml?faces-redirect=true";
    }

    public String logout() {
        return loginManagedBean.userLogOut();
    }

    public String goToHistory() {
        return "/secured/logsHistory.xhtml?faces-redirect=true";
    }


    public void addOperand(String input) {
        if (StringUtils.isEmpty(operator) && StringUtils.isEmpty(operand)) {
            result = new BigDecimal(0);
            expression = "";
        }

        if (!StringUtils.equals(".", input) || !StringUtils.contains(operand, ".")) {
            operand += input;
            expression += input;
        }
    }

    public void addOperation(String input) {
        if (StringUtils.isNotEmpty(expression)) {
            if (StringUtils.isEmpty(operator)) {
                operator = "=";
            }

            if (StringUtils.isNotEmpty(operator) && StringUtils.isNotEmpty(operand)) {
                result = calculatorManagedBean.getCalculateResult(operator, operand, result);
                if (isUserLoggedIn()) {
                    logs = logManagedBean.getLogs(expression, result.toString());
                }
                operand = "";
                operator = null;
                expression = "" + result;
            }

            if (!StringUtils.equals("=", input) && !StringUtils.equals("-", expression)) {
                operator = input;
                expression = "" + result + " " + operator + " ";
            }
        }
    }

    public void reset() {
        expression = "";
        operand = "";
        operator = null;
        result = BigDecimal.valueOf(0);
    }

    public String getUserName() {
        user = loginManagedBean.getCalcUser();
        return (user == null || user.getId() == null) ? "Guest" : user.getUserName();
    }

    public boolean isUserLoggedIn() {
        CalcUser user = loginManagedBean.getCalcUser();
        return user != null && user.getId() != null;
    }

    public void saveLogWithName() {
        editMode = true;
    }

    public void clearLogs() {
        logManagedBean.setLog(new StringBuilder());
        logs = null;
    }

    public void saveLog() {
        if (StringUtils.isEmpty(logName)) {
            ErrorMessageUtils.createErrorMessage(null, "Enter log name");
        } else if (StringUtils.isEmpty(logs)) {
            ErrorMessageUtils.createErrorMessage(null, "Cannot save empty logs");
        } else {
            boolean saved = logManagedBean.saveLog(user.getId(), logName, logs.toString());
            if (saved) {
                clearLogs();
                editMode = false;
            }
        }
    }

    public void cancel() {
        logName = null;
        editMode = false;
    }

    public CalcUser getUser() {
        return user;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public StringBuilder getLogs() {
        return logs;
    }

    public void setLogs(StringBuilder logs) {
        this.logs = logs;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
        this.loginManagedBean = loginManagedBean;
    }

    public void setCalculatorManagedBean(CalculatorManagedBean calculatorManagedBean) {
        this.calculatorManagedBean = calculatorManagedBean;
    }

    public void setLogManagedBean(LogManagedBean logManagedBean) {
        this.logManagedBean = logManagedBean;
    }
}
