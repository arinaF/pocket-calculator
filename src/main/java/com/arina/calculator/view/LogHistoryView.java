package com.arina.calculator.view;

import com.arina.calculator.bean.LogManagedBean;
import com.arina.calculator.bean.LoginManagedBean;
import com.arina.calculator.model.CalcLog;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class LogHistoryView {

    @ManagedProperty(value = "#{logManagedBean}")
    LogManagedBean logManagedBean;

    @ManagedProperty(value = "#{loginManagedBean}")
    LoginManagedBean loginManagedBean;

    private List<CalcLog> userSavedLogs;

    @PostConstruct
    public void init() {
        userSavedLogs = getSavedLogs();
    }

    public List<CalcLog> getSavedLogs() {
        Long userId = loginManagedBean.getCalcUser().getId();
        return logManagedBean.getUserLogs(userId);
    }

    public String backToCalculator() {
        return "/public/index.xhtml?faces-redirect=true";
    }

    public List<CalcLog> getUserSavedLogs() {
        return userSavedLogs;
    }

    public void setUserSavedLogs(List<CalcLog> userSavedLogs) {
        this.userSavedLogs = userSavedLogs;
    }

    public void setLogManagedBean(LogManagedBean logManagedBean) {
        this.logManagedBean = logManagedBean;
    }

    public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
        this.loginManagedBean = loginManagedBean;
    }
}
