package com.arina.calculator.view;

import com.arina.calculator.bean.SignUpManagedBean;
import com.arina.calculator.utils.ErrorMessageUtils;
import org.apache.commons.lang3.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class SignUpView {

    private String username;
    private String password;

    @ManagedProperty(value = "#{signUpManagedBean}")
    private SignUpManagedBean signUpManagedBean;

    public String createNewUser() {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            ErrorMessageUtils.createErrorMessage(null, "Please enter your username and password!");
            return null;
        }
        return signUpManagedBean.createNewUser(username, password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSignUpManagedBean(SignUpManagedBean signUpManagedBean) {
        this.signUpManagedBean = signUpManagedBean;
    }
}
