package com.arina.calculator.bean;

import com.arina.calculator.dao.CalcLogDao;
import com.arina.calculator.model.CalcLog;
import org.apache.commons.lang3.time.DateFormatUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class LogManagedBean implements Serializable {

    private CalcLogDao calcLogDao = new CalcLogDao();

    private StringBuilder log = new StringBuilder();
    private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    public StringBuilder getLogs(String expression, String result) {
        log.append(DateFormatUtils.format(new Date(), DATE_FORMAT));
        log.append(" | ");
        log.append(expression);
        log.append(" = ");
        log.append(result);
        log.append("\n");
        return log;
    }

    public boolean saveLog(Long userId, String logName, String logValue) {
        CalcLog log = new CalcLog();
        log.setUserId(userId);
        log.setLogName(logName);
        log.setLogValue(logValue);
        return calcLogDao.saveNewLog(log);
    }

    public StringBuilder getLog() {
        return log;
    }

    public List<CalcLog> getUserLogs(Long userId) {
        return calcLogDao.getLogsList(userId);
    }

    public void setLog(StringBuilder log) {
        this.log = log;
    }
}
