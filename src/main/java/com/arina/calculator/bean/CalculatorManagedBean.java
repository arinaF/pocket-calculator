package com.arina.calculator.bean;

import org.apache.commons.lang3.StringUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.math.BigDecimal;

@ManagedBean
@ViewScoped
public class CalculatorManagedBean implements Serializable {

    public BigDecimal getCalculateResult(String operator, String operand, BigDecimal result) {

        BigDecimal currentOperand = new BigDecimal(operand);
        if (StringUtils.equals("=", operator)) {
            result = currentOperand;
        } else if (StringUtils.equals("+", operator)) {
            result = result.add(currentOperand);
        } else if (StringUtils.equals("-", operator)) {
            result = result.subtract(currentOperand);
        } else if (StringUtils.equals("*", operator)) {
            result = result.multiply(currentOperand);
        } else if (StringUtils.equals("/", operator)) {
            try {
                result = result.divide(currentOperand);
            } catch (ArithmeticException ex) {
                ex.printStackTrace();
                result = result.divide(currentOperand, 10, BigDecimal.ROUND_HALF_UP);
            }
        }

        return result;
    }
}
