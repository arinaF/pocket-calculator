package com.arina.calculator.bean;

import com.arina.calculator.dao.CalcUserDao;
import com.arina.calculator.model.CalcUser;
import com.arina.calculator.utils.ErrorMessageUtils;
import com.arina.calculator.utils.PasswordUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class SignUpManagedBean implements Serializable {

    private CalcUserDao calcUserDao = new CalcUserDao();
    private CalcUser calcUser = new CalcUser();

    @ManagedProperty(value = "#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;

    public String createNewUser(String username, String password) {
        CalcUser calcUser = calcUserDao.getUser(username);
        if (calcUser != null) {
            ErrorMessageUtils.createErrorMessage(null, "Username " + username + " already exists. Please choose another username");
            return null;
        }
        calcUser = new CalcUser();
        String hashedPassword = PasswordUtils.hashPassword(password);
        calcUser.setUserName(username);
        calcUser.setPassword(hashedPassword);
        calcUserDao.createUser(calcUser);
        return loginManagedBean.userLogIn(username, password);
    }

    public CalcUser getCalcUser() {
        return calcUser;
    }

    public void setCalcUser(CalcUser calcUser) {
        this.calcUser = calcUser;
    }

    public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
        this.loginManagedBean = loginManagedBean;
    }
}
