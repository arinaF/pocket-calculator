package com.arina.calculator.bean;

import com.arina.calculator.dao.CalcUserDao;
import com.arina.calculator.model.CalcUser;
import com.arina.calculator.utils.ErrorMessageUtils;
import com.arina.calculator.utils.PasswordUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class LoginManagedBean implements Serializable {

    private CalcUserDao calcUserDao = new CalcUserDao();
    private CalcUser calcUser = new CalcUser();

    public String userLogIn(String username, String password) {
        calcUser = calcUserDao.getUser(username);

        if (calcUser == null) {
            calcUser = new CalcUser();
            ErrorMessageUtils.createErrorMessage(null, "User not found");
            return null;
        } else {
            boolean isCorrect = PasswordUtils.checkPassword(password, calcUser.getPassword());
            if (isCorrect) {
                calcUserDao.updateUser(calcUser);
                return "/public/index.xhtml?faces-redirect=true";
            } else {
                ErrorMessageUtils.createErrorMessage(null, "You have entered an invalid username or password");
                return null;
            }
        }
    }

    public String userLogOut() {
        calcUser = null;
        return "/public/index.html?faces-redirect=true";
    }

    public CalcUser getCalcUser() {
        return calcUser;
    }

    public void setCalcUser(CalcUser calcUser) {
        this.calcUser = calcUser;
    }
}
