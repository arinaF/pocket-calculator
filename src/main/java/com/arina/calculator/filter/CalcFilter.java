package com.arina.calculator.filter;

import com.arina.calculator.bean.LoginManagedBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CalcFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LoginManagedBean loginManagedBean =
                (LoginManagedBean) ((HttpServletRequest) servletRequest).getSession()
                        .getAttribute("loginManagedBean");

        if (loginManagedBean == null || loginManagedBean.getCalcUser() == null || loginManagedBean.getCalcUser().getId() == null) {
            String contextPath = ((HttpServletRequest) servletRequest).getContextPath();
            ((HttpServletResponse) servletResponse).sendRedirect(contextPath + "/public/index.xhtml");
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
