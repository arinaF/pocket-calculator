package com.arina.calculator.dao;

import com.arina.calculator.model.CalcUser;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.Date;

public class CalcUserDao extends AbstractDao {

    public CalcUser getUser(String userName) {
        try {
            EntityManager manager = getEntityManager();
            CriteriaBuilder cb = manager.getCriteriaBuilder();

            CriteriaQuery<CalcUser> query = cb.createQuery(CalcUser.class);
            Root<CalcUser> calcUser = query.from(CalcUser.class);
            ParameterExpression<String> userNameParameter = cb.parameter(String.class, "userName");
            query.select(calcUser).where(cb.equal(calcUser.get("userName"), userNameParameter));
            return manager.createQuery(query).setParameter("userName", userName).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public boolean createUser(CalcUser calcUser) {
        return saveNewEntry(calcUser);
    }

    public boolean updateUser(CalcUser calcUser) {
        calcUser.setLastLogin(new Date());
        return updateEntry(calcUser);
    }
}
