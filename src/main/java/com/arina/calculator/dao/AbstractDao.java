package com.arina.calculator.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public abstract class AbstractDao {

    public static final EntityManager manager;

    static {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("calcdb");
        manager = factory.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return manager;
    }

    protected boolean saveNewEntry(Object object) {
        EntityTransaction transaction = null;
        EntityManager manager = getEntityManager();
        try {
            transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(object);
            manager.flush();
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return false;
        }
    }

    protected boolean updateEntry(Object object) {
        EntityManager manager = getEntityManager();
        EntityTransaction transaction = null;
        try {
            transaction = manager.getTransaction();
            transaction.begin();
            manager.merge(object);
            transaction.commit();
            return true;
        } catch (Exception ex) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return false;
        }
    }
}
