package com.arina.calculator.dao;

import com.arina.calculator.model.CalcLog;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CalcLogDao extends AbstractDao {

    public List<CalcLog> getLogsList(Long userId) {
        try {
            EntityManager manager = getEntityManager();
            CriteriaBuilder cb = manager.getCriteriaBuilder();
            CriteriaQuery<CalcLog> query = cb.createQuery(CalcLog.class);
            Root<CalcLog> calcLog = query.from(CalcLog.class);
            ParameterExpression<Long> userIdParameter = cb.parameter(Long.class, "userId");
            query.select(calcLog).where(cb.equal(calcLog.get("userId"), userIdParameter));
            return manager.createQuery(query).setParameter("userId", userId).getResultList();
        } catch (NoResultException ex) {
            return new ArrayList<CalcLog>();
        }
    }

    public boolean saveNewLog(CalcLog calcLog) {
        calcLog.setCreated(new Date());
        return saveNewEntry(calcLog);
    }
}
