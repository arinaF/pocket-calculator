## Pocket Calculator application ##

### Application functionality: ###
* Calculator for all users.
* Calculation logs viewing and saving for authenticated and authorized users.
* User creation.

### Running the application ###
* Clone project from https://arinaF@bitbucket.org/arinaF/pocket-calculator.git
* Open application in IDE and open file 'persistence.xml'
* Specify properties:

```
#!xml

javax.persistence.jdbc.url 
javax.persistence.jdbc.user
javax.persistence.jdbc.password
```

* Execute following scripts in PostgreSQL DB:


```
#!sql

CREATE TABLE CALC_USERS (
	ID SERIAL PRIMARY KEY NOT NULL,
	USER_NAME VARCHAR(100) NOT NULL,
	PASSWORD VARCHAR(100) NOT NULL,
	LAST_LOGIN DATE 
);

CREATE TABLE CALC_LOGS (
	ID SERIAL PRIMARY KEY NOT NULL,
	USER_ID INTEGER REFERENCES CALC_USERS(ID) NOT NULL ,
	LOG_NAME VARCHAR(100) NOT NULL,
	LOG_VALUE TEXT,
	CREATED DATE NOT NULL
);
```

* Run application via maven tomcat7 configuration - mvn tomcat7:run-war, using the development profile, specified in pom.xml.